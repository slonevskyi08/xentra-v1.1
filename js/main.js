window.onload = function () {
    let loginBtn = document.querySelector('.btn-login');
    let loginBtnStyle = window.getComputedStyle(loginBtn);
    let loginPage = document.querySelector('.login-page');
    let loginPageStyle = window.getComputedStyle(loginPage);
    let loginPageForm = document.querySelector('.login-page form');

    let loginBtnOldStyle = loginBtnStyle.color;

    loginBtn.onclick = function () {
        if (loginPage.style.opacity === `1`) {
            loginPage.style.right = `-200px`;
            loginPage.style.opacity = `0`;
            timeToClose(loginPageForm, `none`);
            loginBtn.style = loginBtnOldStyle;
        } else {

            if(window.matchMedia(`(max-width:991px)`).matches) {
                loginPageForm.style.display = `block`;
                loginPage.style.right = `15px`;
                loginPage.style.opacity = `1`;
                loginBtn.style.color = `#c3c3c3`;
            } else {
                loginPageForm.style.display = `block`;
                loginPage.style.right = `50px`;
                loginPage.style.opacity = `1`;
                loginBtn.style.color = `#c3c3c3`;
            }
        }
    };

    function timeToClose(element, value) {
        setTimeout(function () {
            return element.style.display = value;
        }, 500)
    }


    let social = document.querySelector(`.social ul`);
    let socialStyle = window.getComputedStyle(social);
    let btnSocial = document.querySelector(`.btn-social`);
    let btnSocialStyle = window.getComputedStyle(btnSocial);

    let btnSocialStyleOld = btnSocialStyle;

    btnSocial.onclick = function () {
        if (social.style.opacity == 0) {
            social.style.opacity = `1`;
            social.style.width = `110px`;
            social.style.zIndex = `999`;
            btnSocial.style.color = `#c3c3c3`;
            btnSocial.style.borderColor = `#c3c3c3`;
            btnSocial.style.background = `#fff`;
        } else {
            social.style.opacity = `0`;
            social.style.width = `0`;
            social.style.zIndex = `-1`;
            btnSocial.style = btnSocialStyleOld;
        }
    };
};

function initMap() {
    let uluru = {lat: 50.462809, lng: 30.517577};
    let uluruMarker = {lat: 50.462648, lng: 30.516057};
    let map = new google.maps.Map(document.querySelector('div.map'), {
        zoom: 17,
        center: uluru
    });
    let marker = new google.maps.Marker({
        position: uluruMarker,
        map: map
    });
}